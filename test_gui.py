#------------------------------------------------------------------------------------------------------------------------------------
# test_gui.py
# Created: 01.05.2021
# Author:  Carl Zech
#------------------------------------------------------------------------------------------------------------------------------------
from PyQt5 import uic
from PyQt5.QtWidgets import QMainWindow, QApplication, QButtonGroup, QWidget,QVBoxLayout, QLabel
import sys
from PyQt5.QtCore import QObject, pyqtSignal, QRunnable, pyqtSlot, QThreadPool, Qt  
import serial 
from PyQt5 import QtWidgets, QtCore
import json
from pyqtgraph import PlotWidget, plot
import pyqtgraph as pg
#------------------------------------------------------------------------------------------------------------------------------------
#  cd "Documents\Studium RV\Projektarbeit\PythonTestCodes"

ser = serial.Serial(port="COM4", baudrate=57600, bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE)
#------------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------------
class WorkerSignals(QObject):
    # creates the signal result which can transmit any object
    result = pyqtSignal(object)  
#------------------------------------------------------------------------------------------------------------------------------------
class Worker(QRunnable):
    def __init__(self, fn, *args, **kwargs):
        # Store constructor arguments (re-used for processing)
        super(Worker, self).__init__()                                                                              
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()    
        # Adds the callback to the kwargs
        self.kwargs['signals'] = self.signals                                                                       

    @pyqtSlot()
    def run(self):
        # calls the function and hands over *self.args (any argument) and self.kwargs (the signals) 
        result = self.fn(*self.args, **self.kwargs)   
        # emits the signal result back to function which called the worker                                                              
        if(status.threadstatus == True):
            self.signals.result.emit(result)   
            
#------------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------------
def data_collecting(signals):
    while True:
        # checks if there are any new data
        if ser.in_waiting > 0:    
            # reads a whole line out of the serial port, decodes it and saves it in the variable "line"                                                                             
            line = ser.readline().decode("ASCII")                                                               
            print(line)
            # emits the line back to the function which called this function
            signals.result.emit(line)     
            
#------------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------------
class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        uic.loadUi("test.ui", self)
        
        self.setWindowTitle("Leistungsprüfstand")
        
        self.setGeometry(800, 200, 2500, 900)
        #self.setFixedSize(1150, 750)
        
        self.btn_start.clicked.connect(self.start)
        
        self.threadpool = QThreadPool()
        
        ser.flushInput()
        
        self.force = []
        self.time = []
    
        #Add Background colour to black
        self.graphWidget.setBackground('k')
        # Add Title
        self.graphWidget.setTitle("Zeit-Masse-Diagramm", color="w", size="18pt")
        # Add Axis Labels
        styles = {"color": "#f00", "font-size": "20px"}
        self.graphWidget.setLabel("left", "Masse (g)", **styles)
        self.graphWidget.setLabel("bottom", "Zeit (s)", **styles)
        #Add legend
        self.graphWidget.addLegend()
        #Add grid
        self.graphWidget.showGrid(x=True, y=True)
        #Set Range
        self.graphWidget.setXRange(0, 80, padding=0)
        self.graphWidget.setYRange(-100, 1100, padding=0)

        pen = pg.mkPen(color=(255, 255, 255), width=3)
        #self.graphWidget.plot(self.time, self.force, pen=pen)
        self.data_line = self.graphWidget.plot(self.time, self.force, pen=pen)
        
#------------------------------------------------------------------------------------------------------------------------------------
    def update_plot_data(self,force,time):
        self.force.append(force)
        self.time.append(time)
        # Update the data
        self.data_line.setData(self.time, self.force)
        force = "%.2f" % round(force,2) 
        self.value.setText(str(force)) 
        

#------------------------------------------------------------------------------------------------------------------------------------
    def start(self):
        self.worker1 = Worker(data_collecting)
        self.worker1.signals.result.connect(self.process_data)
        self.threadpool.start(self.worker1)

#------------------------------------------------------------------------------------------------------------------------------------        
    def process_data(self, value):
        value = json.loads(value)
        self.update_plot_data(-(value["r"]+45), value["t"]/1000)
        
#------------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------------
def main():
    # Create PyQT5 Application object and hand off given command line arguments
    app = QApplication(sys.argv)   
    # Set style from standard list (should be available on most common OS)                                                                                 
    app.setStyle("Fusion") 
    # Create main window object (using GUI designer template)                                                                                   
    window = MainWindow()  
    # Instantiate window and load it into memory                                                                                         
    window.show()   
    # Enter QT5 application main loop and start handling QT5 events
    # Use sys.exit to ensure clean exit and to inform the host environment 
    # how the Application was closed                                                                                                
    sys.exit(app.exec_())    
                                                                                           
#------------------------------------------------------------------------------------------------------------------------------------ 
if __name__ == "__main__":
    main()
#------------------------------------------------------------------------------------------------------------------------------------