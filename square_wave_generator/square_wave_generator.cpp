// Do not remove the include below
#include <Arduino.h>
#include "square_wave_generator.h"


int ledPin = 11; //Output pin
float d = 0.025; //Time to approximately reach 30 sec. for 1 cycle (25Hz-1000Hz)


void setup()
{
	pinMode(ledPin, OUTPUT);

}

void loop()
{
	for(float i = 20; i >= 0.5; i = i-d){
		digitalWrite(ledPin, HIGH);
		delay(i);
		digitalWrite(ledPin, LOW);
		delay(i);
	}
	for(float i = 0.5; i <= 20; i = i+d){
		digitalWrite(ledPin, HIGH);
		delay(i);
		digitalWrite(ledPin, LOW);
		delay(i);

	}
}
