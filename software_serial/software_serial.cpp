#include "HX711.h"
#include "ArduinoJson.h"

// HX711 circuit wiring
const int LOADCELL_DOUT_PIN = 4;
const int LOADCELL_SCK_PIN = 5;
const byte interruptPin = 2;

volatile unsigned long time;

HX711 scale;

void duty_cycle(){
	time = millis();
}

void setup() {
  Serial.begin(57600);
  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);

  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), duty_cycle, RISING);

  scale.set_scale(44.f);    // this value is obtained by calibrating the scale with known weights; see the README for details
  scale.tare();               // reset the scale to 0
}

void loop() {
  StaticJsonDocument<30> testDocument;
  char buffer[30];
  testDocument["r"] = scale.get_units();
  testDocument["t"] = time;
  serializeJson(testDocument, buffer);

  Serial.println(buffer);
  delay(1);
}
