// Do not remove the include below
#include "simulate_square.h"

const int wave_pin = 12;

void setup() {

}

void loop() {
	static bool value = LOW;
	pinMode(wave_pin, OUTPUT);
	digitalWrite(wave_pin,value);
	if (value == LOW) {
			value = HIGH;
	} else {
		value = LOW;
	}
	delay(100);
}
